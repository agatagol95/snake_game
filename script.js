const cvs = document.getElementById("myCanvas");
const ctx = cvs.getContext('2d');

let box =  25;

let snake = [];
snake[0] = {
    x: 9*box,
    y: 9*box
}
let d;

let scoreBackground = new Image();
scoreBackground.src = "scoreBackground.jpg";

let background = new Image();
background.src="green.png";

let foodImg = new Image();
foodImg.src = "food.png";
let foodPos = {
    x: (Math.floor(Math.random()*10) *50) + 3*box,
    y: (Math.floor(Math.random()*10) *50) + 3*box
}

let gameOver = "GAME OVER";
let isGameOn = true;
let score = 0;

function collision(head,array){
    for(let i = 0; i < array.length; i++){
        if(head.x == array[i].x && head.y == array[i].y){
            return true;
        }
    }
    return false;
}

document.addEventListener("keydown", event => {
    if(event.keyCode == 37) {
        if(d == "right") {
            return d;
        } else { 
            d = "left";
        }
    } else if(event.keyCode == 38) { 
        if(d == "down") {
            return d;
        }else {
            d = "up";
        }
    } else if(event.keyCode == 39) {
        if(d == "left") {
            return d;
        }else {
            d = "right";
        }
    } else if(event.keyCode == 40) {
        if(d == "up"){
            return d;
        }else {
            d = "down";
        }
    }
});
document.addEventListener("click", function(event) {
    let x = event.pageX;
    let y = event.pageY;
    if(!isGameOn) {
        if(x > 9.5*box && x < 15.5*box && y > 9*box && y < 10.5*box) {
            this.location.reload();
        }
    } else return "";
})
function reload() {
    ctx.fillStyle = "red";
    ctx.fillRect(9.5*box, 9*box, 6*box, 1.5*box);
    ctx.fillStyle = "white";
    ctx.font = "20px Changa one";
    ctx.fillText("PLAY AGAIN", 10*box, 10*box);
};
setTimeout(reload(), 10000);
function draw() { 
    ctx.drawImage(scoreBackground, 0, 0, 25*box, 25*box);
    ctx.drawImage(background, 2*box, 2*box, 21*box, 21*box);
    ctx.drawImage(foodImg, 2*box, 0.5*box, box, box);

    for(let i=0; i<snake.length; i++) {
        ctx.fillStyle = (i == 0) ? "white":"black";
        ctx.fillRect(snake[i].x,snake[i].y, box, box);
    }
    ctx.drawImage(foodImg, foodPos.x, foodPos.y, box, box);
    
    let snakeX = snake[0].x;
    let snakeY = snake[0].y;
    
    if(d == "left") {
        snakeX -= box;
    } else if(d == "up") {
        snakeY -= box;
    } else if(d == "right") {
        snakeX += box;
    } else if(d == "down") {
        snakeY += box;
    }
    
    if(snakeX == foodPos.x && snakeY == foodPos.y) {
        foodPos.x = (Math.floor(Math.random()*10) *50) + 3*box;
        foodPos.y = (Math.floor(Math.random()*10) *50) + 3*box;
        score ++;
    } else {
        snake.pop();
    }
    let newHead = {
        x: snakeX,
        y: snakeY
    }
    if(snakeX < 2*box || snakeX > 22*box || snakeY < 2*box || snakeY > 22*box || collision(newHead,snake)) {
        clearInterval(game);
        ctx.fillStyle = "white";
        ctx.font = "70px Changa one";
        ctx.fillText(gameOver, 4*box, 8*box);
        reload();
        isGameOn = false;
    } else console.log("game on");
    
    snake.unshift(newHead);
    
    ctx.fillStyle = "white";
    ctx.font = "25px Changa one";
    ctx.fillText(score, 3.2*box, 1.4*box);
    
}
let game = setInterval(draw, 100);